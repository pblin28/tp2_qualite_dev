public class Auteur{
    private String nom;
    private String citation_tragedie;
    private int qualité_tragedie;
    private String citation_comedie;
    private int qualité_comedie;
    private String citation_dramatique;
    private int qualité_dramatique;

    public Auteur(String nom, int tragedie, String citation_tra, int comedie, String citation_com, int dramatique, String citation_drama) {
        this.nom = nom;
        this.citation_tragedie = citation_tra;
        this.qualité_tragedie = tragedie;
        this.citation_comedie = citation_com;
        this.qualité_comedie = comedie;
        this.citation_dramatique = citation_drama;
        this.qualité_dramatique = dramatique;
    }

    public int getQualitéTragédie() {
        return this.qualité_tragedie;
    }



}